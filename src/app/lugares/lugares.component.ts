import { Component } from '@angular/core';
import { LugaresServices } from '../services/lugares.services';

@Component({
  selector: 'app-lugares',
  templateUrl: './lugares.component.html'
})

export class LugaresComponent {
  title = 'Square';
  

  lat:number = 4.6560663;
  lng:number = -74.0595918;
  lugares = null;
  constructor(private lugaresServices: LugaresServices) {
    //this.lugares = lugaresServices.getLugares();
    lugaresServices.getLugares()
      .valueChanges().subscribe(lugares=>{
        this.lugares = lugares;
      });
  }
}
