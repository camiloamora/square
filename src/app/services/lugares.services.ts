import {Injectable} from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database/database';
import {Http} from '@angular/http';

@Injectable()

export class LugaresServices{
    lugares: any= [
        {id:1, plan: 'pagado', cercania: 1, distancia: 1.5, active: false, nombre: 'Festy tortas'},
        {id:2, plan: 'free', cercania: 1, distancia: 1, active: true, nombre: 'Electricos silver'},
        {id:3, plan: 'pagado', cercania: 2, distancia: 2, active: true, nombre: 'Avisos la ley'},
        {id:4, plan: 'pagado', cercania: 3, distancia: 10, active: false, nombre: 'Surtimax'},
        {id:5, plan: 'free', cercania: 3, distancia: 15, active: true, nombre: 'Drogas Bogotá'}
        ];

    constructor(private afDB:AngularFireDatabase, private http: Http){

    }
    
    public getLugares(){
        //return this.lugares;
        return this.afDB.list('lugares/');
    }

    public buscarLugar(id){
        return this.lugares.filter((lugar) => { return lugar.id == id})[0] || null;
    }

    public guardarLugar(lugar){
        this.afDB.database.ref('lugares/'+lugar.id).set(lugar);
    }

    public obtenerGeoData(direccion){
        //http://maps.google.com/maps/api/geocode/json?address=9-55+calle+72,+Bogota,Colombia
        return this.http.get('http://maps.google.com/maps/api/geocode/json?address='+direccion);
    }
}