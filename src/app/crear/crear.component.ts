import { Component } from '@angular/core';
import { LugaresServices } from "../services/lugares.services";

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html'
})

export class CrearComponent {
    lugar:any = {};

    constructor(private lugaresServices: LugaresServices){

    }

    guardarLugar(){
      //generaciòn de autonumerico
      this.lugar.id = Date.now();
      //se obtiene la información de la geolocalización
      var direccion = this.lugar.calle + ',' + this.lugar.ciudad + ',' +  this.lugar.pais;
      this.lugaresServices.obtenerGeoData(direccion)
        .subscribe((result) => {
          debugger;
          this.lugar.lat = result.json().results[0].geometry.location.lat;
          this.lugar.lng = result.json().results[0].geometry.location.lng;
          this.lugaresServices.guardarLugar(this.lugar); 
          alert('Negocio guardado con exito');
          this.lugar = {};
        });
    }
}
