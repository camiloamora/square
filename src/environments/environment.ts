// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBCjkZKgfnkKnm6J3t__uhIVrjKuDiIvWA",
    authDomain: "square-1511837847670.firebaseapp.com",
    databaseURL: "https://square-1511837847670.firebaseio.com",
    projectId: "square-1511837847670",
    storageBucket: "square-1511837847670.appspot.com",
    messagingSenderId: "178847130111"
  }
};